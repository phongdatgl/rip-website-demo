<?php
ob_start();
?>
<!-- start gallery -->
             <div class="con_gallery">
            <div class="box_gallery" id="lnk_fac">
    <h4>館内</h4>
    <ul>
      <li><a href="images/pop_fac01.jpg" rel="fac" title="外観"><img src="images/thumb_fac01.jpg" alt="外観"></a>外観</li>
      <li><a href="images/pop_fac02.jpg" rel="fac" title="外観"><img src="images/thumb_fac02.jpg" alt="外観"></a>外観</li>
      <li><a href="images/pop_fac03.jpg" rel="fac" title="玄関"><img src="images/thumb_fac03.jpg" alt="玄関"></a>玄関</li>
      <li><a href="images/pop_fac04.jpg" rel="fac" title="癒し処"><img src="images/thumb_fac04.jpg" alt="癒し処"></a>癒し処 夢語</li>
      <li><a href="images/pop_fac05.jpg" rel="fac" title="アロマセラピー"><img src="images/thumb_fac05.jpg" alt="アロマセラピー"></a>アロマセラピー</li>
    </ul>
  </div>
  <div class="box_gallery" id="lnk_room">
    <h4>客室</h4>
    <ul>
      <li><a href="images/pop_room01.jpg" rel="room" title="和洋室"><img src="images/thumb_room01.jpg" alt="和洋室"></a>和洋室</li>
      <li><a href="images/pop_room02.jpg" rel="room" title="客室内露天風呂"><img src="images/thumb_room02.jpg" alt="客室内露天風呂"></a>客室内露天風呂</li>
      <li><a href="images/pop_room03.jpg" rel="room" title="ダブル洋室"><img src="images/thumb_room03.jpg" alt="お抹茶セット"></a>ダブル洋室</li>
      <li><a href="images/pop_room04.jpg" rel="room" title="落ち着いたしつらえ"><img src="images/thumb_room04.jpg" alt="湯かご"></a>落ち着いたしつらえ</li>
      <li><a href="images/pop_room05.jpg" rel="room" title="和洋室"><img src="images/thumb_room05.jpg" alt="湯かご"></a>湯かご</li>
      <li><a href="images/pop_room06.jpg" rel="room" title="夜の露天風呂"><img src="images/thumb_room06.jpg" alt="夜の露天風呂"></a>夜の露天風呂</li>     
    </ul>
  </div>
  <div class="box_gallery" id="lnk_spa">
    <h4>温泉</h4>
    <ul>
      <li><a href="images/pop_spa01.jpg" rel="spa" title="大露天風呂 篝火"><img src="images/thumb_spa01.jpg" alt="大露天風呂 篝火"></a>大露天風呂 篝火</li>
      <li><a href="images/pop_spa02.jpg" rel="spa" title="湯の間 半露天風呂"><img src="images/thumb_spa02.jpg" alt="湯の間 半露天風呂"></a>湯の間 半露天風呂</li>
      <li><a href="images/pop_spa03.jpg" rel="spa" title="信楽風呂(男)"><img src="images/thumb_spa03.jpg" alt="信楽風呂(男)"></a>信楽風呂(男)</li>
      <li><a href="images/pop_spa04.jpg" rel="spa" title="大浴場"><img src="images/thumb_spa04.jpg" alt="大浴場"></a>大浴場</li>
      <li><a href="images/pop_spa05.jpg" rel="spa" title="貸切露天風呂 満天"><img src="images/thumb_spa05.jpg" alt="貸切露天風呂 満天"></a>貸切露天風呂 満天</li>
      <li><a href="images/pop_spa06.jpg" rel="spa" title="貸切露天風呂 花霞"><img src="images/thumb_spa06.jpg" alt="貸切露天風呂 花霞"></a>貸切露天風呂 花霞</li>
      <li><a href="images/pop_spa07.jpg" rel="spa" title="貸切露天風呂 観月"><img src="images/thumb_spa07.jpg" alt="貸切露天風呂 観月"></a>貸切露天風呂 観月</li>
    </ul>
  </div>
  <div class="box_gallery" id="lnk_dish">
    <h4>お料理</h4>
    <ul>
      <li><a href="images/pop_dish01.jpg" rel="dish" title="寿司・しゃぶしゃぶ・天麩羅 風月"><img src="images/thumb_dish01.jpg" alt="寿司・しゃぶしゃぶ・天麩羅 風月"></a>寿司・しゃぶしゃぶ・天麩羅 風月</li>
      <li><a href="images/pop_dish02.jpg" rel="dish" title="和会席 花鳥"><img src="images/thumb_dish02.jpg" alt="和会席 花鳥"></a>和会席 花鳥</li>
      <li><a href="images/pop_dish03.jpg" rel="dish" title="「雪月花」オリジナルのドリンクも"><img src="images/thumb_dish03.jpg" alt="「雪月花」オリジナルのドリンクも"></a>「雪月花」オリジナルのドリンクも</li>
      <li><a href="images/pop_dish04.jpg" rel="dish" title="朝食"><img src="images/thumb_dish04.jpg" alt="朝食"></a>朝食</li>
    </ul>
  </div>
  </div>
<?php 
$page = ob_get_contents();
ob_end_clean();
$page = mb_convert_encoding($page, "utf-8", "euc-jp");
$page = str_replace('euc-jp', 'utf-8', $page);
$fp = fopen('encoding.txt', 'w');
fwrite($fp, $page);
fclose($fp);